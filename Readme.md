##### Please install `git`, `docker` and `docker-compose` before bring up.


Run command: `git clone git@bitbucket.org:leotron13522/testtaskdevops.git && cd testtaskdevops && docker-compose up`


##### First run may take up to 5-10 minutes due to container build and dependencies installation, then need to wait up to 3 minutes until database will be ready.


Related repos: 
- [Django-DRF backend](https://bitbucket.org/leotron13522/testtaskdrf.git)
- [React fronend](https://bitbucket.org/leotron13522/testtaskfrontreact.git)

Selenium tests:
- [Selenium](https://bitbucket.org/leotron13522/selenium_tests_for_users_groups_test_task.git)
